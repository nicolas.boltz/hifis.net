---
date: 2021-03-15
title: Tasks in Mar 2021
service: cloud
---

## Beta Version of Helmholtz Cloud Portal online
The Helmholtz Cloud Portal allows harmonized access to all Helmholtz Cloud services. It contains all necessary information to access the available services. The alpha version of the portal is planned to be available by end 2020 and the publicly accessible beta version is planned for February 2021.



