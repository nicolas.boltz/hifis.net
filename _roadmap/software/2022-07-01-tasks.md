---
date: 2022-07-01
title: Tasks in July 2022
service: software
---

## Initial Prototype of a Helmholtz Research Software Directory

The basis for the software catalogue will be the [Research Software Directory](https://www.research-software.nl/)
developed at [Netherlands eScience Center](https://www.esciencecenter.nl/).
In close collaboration with the original maintainers, an adapted version for Helmholtz will be developed.
The development of the HIFIS Research Software Directory will
be coordinated at GFZ in collaboration with DLR and UFZ, and aided by the HIFIS Software Technology work package.
In this milestone a basic prototype with the initial functionality will be deployed, marking the beginning of the test phase with selected test users.
