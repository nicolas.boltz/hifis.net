---
date: 2022-09-30
title: Tasks in September 2022
service: software
---

## Empower CI/CD #2 - GitLab CI as a Service with at least 3 more partners

The GitLab CI as a Service offer is planned to be expanded to at least 3 more partners within the Helmholtz association.
