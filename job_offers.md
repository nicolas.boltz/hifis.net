---
title: Careers
title_image: default
layout: default
excerpt:
    Job offers related to HIFIS
redirect_from: jobs
---

# Job Offers related to HIFIS
{:.text-success}

* DESY Hamburg: [<i class="fas fa-external-link-alt"></i> **Head of the DESY Computer Centre and Leading Scientist in the Field of Computer and Data Science (W3 level)**](https://v22.desy.de/e67416/records159568/index_eng.html)
    - You will head all aspects of the Computing Division of DESY in Hamburg
    - Shape the future of scientific computing at DESY
    - Drive the development of data science as a key scientific activity at DESY
    - Lead the continuing development of the DESY computing center into a large scale modern compute infrastructure and drive its ambition to transform into a green compute infrastructure for a more sustainable future configuration
    - Leverage the large expertise available in the central computing group and among the DESY scientists, and position DESY as a major player in the field both at the national and international level
    - Responsibility for the support and further development of all IT services for more than 8000 user accounts, incl. the support of office computing

    Application deadline: June 01, 2022.

---

# Further offers

* [Data Science Jobs at Helmholtz](https://www.helmholtz-hida.de/en/jobs/job-offers/)
* [Further vacancies within the Helmholtz Association](https://www.helmholtz.de/en/jobs_talent/job_vacancies/)
