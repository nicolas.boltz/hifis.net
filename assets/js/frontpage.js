document.addEventListener('DOMContentLoaded', function() {
    var toasts = document.getElementsByClassName("toast");
    for (i = 0; i < toasts.length; i++) {
        toasts[i].classList.add("show");
    }
}, false);

function removeToast(id){
    toast = document.getElementById(id);
    if(toast){
        toast.remove();
    }
}
