<!--
###############################################################################
This is the issue template for adding new Software Spotlights.

Notes
-----
* please do not alter the assignments and labels.
* please add only one spotlight per merge request.

If you are looking for instructions how to add a software spotlight, please
visit https://hifis.net/CONTRIBUTING.html#adding-software-spotlights


Note about the preview app
--------------------------
Please provide us a direct link to the spotlight in the preview app. This
reduces the effort for all reviewers. A template is found below. Please replace
BRANCH_NAME with the name of the branch you are merging from, and <SLUG> with
the slug of your spotlight. Alternatively, wait until the pipeline finished
building the preview, and copy-paste the link here.

Credentials for preview app
---------------------------

The credentials for the preview app are (they are publicly available, so feel free to share them with your stakeholders):

User: hifis
Password: HIFISReview!
###############################################################################
-->

<!-- Delete this line and tell us about the spotlight -->

* :point_right: **[Review link](https://hifis-review-app.hzdr.de/review-apps/hifis-overall-hifis-net/<BRANCH_NAME>/spotlights/<SLUG>)**

#### Checklist for authors

* [ ] Add a link pointing to the review app

#### Checklist for reviewers

* [ ] File size of images is not too large
* [ ] Update `date_added` attribute
* [ ] Centres are referenced by official names
* [ ] License is named by SPDX identifier if possible
* [ ] All links are pointing to the right place

/cc @frust45 @konrad @jandt-desy
/assign_reviewer @christian.meessen
/label ~"Software Spotlight"
/label ~"Progress::4_Can review"
