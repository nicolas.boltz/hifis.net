---
layout: spotlight

# Spotlight list attributes
name: OpenGeoSys
preview_image: ogs/ogs_textlogo.png
excerpt: OpenGeoSys (OGS) is a scientific open source project for the development of numerical methods for the simulation of thermo-hydro-mechanical-chemical (THMC) processes in porous and fractured media.

# Title for individual page
title_image: ogs_image.jpg
title: OpenGeoSys
date_added: 2022-06-13
keywords:
    - multi-physics
    - thmc
    - hydrological modeling
    - geothermal modeling
    - geotechnics
    - hpc
    - ogs
hgf_research_field: Earth & Environment
hgf_centers:
    - Helmholtz Centre for Environmental Research (UFZ)
contributing_organisations:
    - name: TU Bergakademie Freiberg
    - name: Bundesanstalt für Geowissenschaften und Rohstoffe
    - name: TU Dresden
scientific_community:
    - Hydrology
    - Geothermal energy
    - Geotechnics
impact_on_community:
contact: info@opengeosys.org
platforms:
    - type: webpage
      link_as: https://www.opengeosys.org/
    - type: gitlab
      link_as: https://gitlab.opengeosys.org/ogs/ogs
    - type: github
      link_as: https://github.com/ufz/ogs
license: BSD-3-clause
costs: Free
software_type:
    - Modelling
    - Simulation
application_type:
    - Desktop
    - HPC
programming_languages:
    - C++
    - Python
doi: 10.5281/zenodo.6405711
funding:
    - shortname: UFZ
      link_as: https://www.ufz.de/
---

# OpenGeoSys - Open-source multi-physics

OpenGeoSys (OGS) is a scientific open source project for the development of numerical methods for the simulation of thermo-hydro-mechanical-chemical (THMC) coupled processes in porous and fractured media. OGS has been successfully applied in the fields of regional, contaminant and coastal hydrology, fundamental and geothermal energy systems, geotechnical engineering, energy storage, CO2 sequestration/storage and nuclear waste management and disposal.

<div class="spotlights-text-image">
<img src="{{ site.directory.images | relative_url}}spotlights/ogs/ogs_image.jpg" alt="Schematic of resolution levels, data, processes and states in mHM.">
<span>Scientists discuss the OpenGeoSys simulation results of a geothermal model in the <a href="https://www.ufz.de/vislab">Visualization Centre of the UFZ</a>.</span>
</div>

OpenGeoSys provides a flexible simulation framework, a wide array of pre- and postprocessing tools to implement comprehensive simulation workflows as well as the possibility to utilize HPC environments for very large simulations runs. It is developed as an [open-source community project](https://gitlab.opengeosys.org/ogs/ogs). Limited support is available at the [community forum](https://discourse.opengeosys.org).
